import datetime
import inotify.adapters
import inotify.constants as const
import os
import Queue
import time as pytime
import ffmpeg

from stat import ST_SIZE
from threading import Thread

LARGE_FILE_SIZE = 500   # In MiB
# TEMP_DIR = '/tmp/file-spooler/'
INPUT_DIR = None
OUTPUT_DIR = None

# Numbers of threads that will process the files.
THREADS_NORMAL_QUEUE = 2
THREADS_LOW_QUEUE = 1

main_queue = Queue.Queue()
bf_queue = Queue.Queue()


def time(iso=None):
    if iso:
        return str(datetime.datetime.now().isoformat())

    return str(datetime.datetime.now().strftime(
        "%Y-%m-%d %H:%M:%S"
    ))


def process_normal():
    while True:
        pytime.sleep(1)
        item = main_queue.get()
        print('[{}][NORMAL][PROCESSING] {}').format(time(), str(item))
        stream = ffmpeg.input(item)
        stream = ffmpeg.filter(stream, 'fps', fps=25, round='up')
        stream = ffmpeg.output(stream, OUTPUT_DIR + '/output.mp4')
        ffmpeg.run(stream)

        main_queue.task_done()


def process_low():
    while True:
        pytime.sleep(5)
        item = main_queue.get()
        # do stuff
        print('[{}][LOW][PROCESSING] {}').format(time(), str(item))
        main_queue.task_done()


def _main():
    current_dir = os.getcwd()
    global INPUT_DIR
    global OUTPUT_DIR

    if not INPUT_DIR:
        INPUT_DIR = os.path.join(current_dir, 'input')
    if not OUTPUT_DIR:
        OUTPUT_DIR = os.path.join(current_dir, 'output')

    i = inotify.adapters.Inotify()
    print("[{}] [WATCHING: {}]").format(time(), INPUT_DIR)
    i.add_watch(INPUT_DIR, const.IN_MOVED_TO)

    # Launch the Threads
    for x in range(THREADS_NORMAL_QUEUE):
        t = Thread(target=process_normal)
        t.daemon = True
        t.start()
    for x in range(THREADS_LOW_QUEUE):
        t = Thread(target=process_low)
        t.daemon = True
        t.start()

    for event in i.event_gen(yield_nones=False):
        (_, type_names, path, filename) = event

        print("[{}] FILENAME=[{}] [ADDED TO QUEUE]").format(
            time(),
            filename,
        )
        file_path = os.path.join(path, filename)
        enqueue(file_path, time(True))


def enqueue(file_path, time):
    file_size_bytes = os.stat(file_path)[ST_SIZE]
    file_size = file_size_bytes / 1024 ** 2
    if file_size > LARGE_FILE_SIZE:
        bf_queue.put(file_path)
    else:
        main_queue.put(file_path)


if __name__ == '__main__':
    _main()
